// Configure Popout by defining these properties. Default values will be used where none are defined.
// Note: If defining midPoint, BOTH 'x' and 'y' MUST BE DEFINED.

window.POPOUT_cfg = {
  hooks: {
    container: 'wrapper',
    canvas: 'depth',
    popout: '.pop'
  },
  gradient: {
    stop: 180
  },
  dimensions: {
    height: 225
  },
  stroke: true, // Apply stroke effect?
  noSides: true
}

document.addEventListener('DOMContentLoaded', function () {
  document.querySelectorAll('a.pop').forEach((el) => {
    el.setAttribute('href', '#')
    el.addEventListener('click', () => {
      let active = document.querySelector('.active')
      if (active) {
        active.classList.remove('active')
      }
      el.classList.add('active')
      let activepage = document.querySelector('.active').getAttribute('id')
      var xhr = new window.XMLHttpRequest()
      xhr.open('GET', `${activepage}.html`, true)
      xhr.onload = () => {
        if ((xhr.status >= 200) && (xhr.status < 400)) {
          let parser = new window.DOMParser()
          let content = parser.parseFromString(xhr.response, 'text/html').getElementById('content')
          let body = document.getElementById('content')
          body.parentNode.replaceChild(content, body)
        } else {
          console.log('error')
        }
      }
      xhr.onerror = () => console.log('damnit')
      xhr.send()
    })
  })
})

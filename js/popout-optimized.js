var popout;
(function () {
  "use strict";

  var $$0 = {
    enumerable: false,
    configurable: true,
    writable: true
  };

  var _$0 = this;

  var _$1 = _$0.ReferenceError;
  var _$2 = _$1.prototype;
  var _$3 = _$0.Object;
  var _$4 = _$3.defineProperty;

  var _4 = function (cfg) {
    'use strict';

    let d = document.documentElement; // Variable defaults. Better to configure these through the initialization script at js/init.js

    const getDocHeight = () => Math.max(d.scrollHeight, d.clientHeight);

    const config = {
      vanishingPoint: cfg.vanishingPoint || {
        x: Math.round(d.clientWidth / 2),
        y: Math.round((window.innerHeight + d.scrollTop) / 2),
        recalc: true
      },
      canvasID: cfg.canvasID || 'depth',
      popoutSelector: cfg.popoutSelector || '.pop',
      height: cfg.height || d.scrollHeight,
      gradientStop: cfg.gradientStop || 60,
      stroke: cfg.stroke || false,
      noSides: cfg.noSides || false // CONSTANTS - DO NOT MODIFY

    };
    const UPPER_LEFT = 0;
    const UPPER_RIGHT = 1;
    const LOWER_RIGHT = 2;
    const LOWER_LEFT = 3;
    const X = 0;
    const Y = 1; // Determine our corner coordinates based off the given dimensions and offsets.

    function setMidPoint() {
      config.vanishingPoint.x = Math.round(d.clientWidth / 2);
      config.vanishingPoint.y = Math.round((window.innerHeight + d.scrollTop) / 2);
    }

    function getCornerVals(el) {
      var corner = new Array(4);

      for (let i = 0; i < 4; i += 1) {
        corner[i] = new Array(2);
      }

      corner[UPPER_LEFT][X] = el.left;
      corner[UPPER_LEFT][Y] = el.top + d.scrollTop;
      corner[UPPER_RIGHT][X] = el.left + el.width;
      corner[UPPER_RIGHT][Y] = el.top + d.scrollTop;
      corner[LOWER_RIGHT][X] = el.left + el.width;
      corner[LOWER_RIGHT][Y] = el.top + el.height + d.scrollTop;
      corner[LOWER_LEFT][X] = el.left;
      corner[LOWER_LEFT][Y] = el.top + el.height + d.scrollTop;
      return corner;
    } // Retrieves and calculates dimensions and offsets of our divs as well as determine what
    // faces get drawn.


    function GetElemProperties(el) {
      var offset = el.getBoundingClientRect();
      this.width = Math.round(el.offsetWidth);
      this.height = Math.round(el.offsetHeight);
      this.left = offset.left + document.body.scrollLeft;
      this.top = offset.top + document.body.scrollTop;
      this.coord = getCornerVals(this);
      this.popColor = window.getComputedStyle(el).backgroundColor; // Determine where corners are in relation to the mid point to
      // determine what sides should be visible.

      if (this.coord[UPPER_LEFT][X] >= config.vanishingPoint.x) this.leftFace = true;
      if (this.coord[UPPER_RIGHT][X] < config.vanishingPoint.x) this.rightFace = true;
      if (this.coord[UPPER_LEFT][Y] > config.vanishingPoint.y) this.topFace = true;
      if (this.coord[LOWER_LEFT][Y] <= config.vanishingPoint.y) this.bottomFace = true; // Determine which corner is closest to the vanishingPoint.

      let distBuff;
      this.distance = Number.MAX_VALUE;

      for (let a = 0; a < 3; a++) {
        distBuff = Math.sqrt(Math.pow(config.vanishingPoint.x - this.coord[a][X], 2) + Math.pow(config.vanishingPoint.y - this.coord[a][Y], 2));
        if (distBuff < this.distance) this.distance = distBuff;
      }
    } // Set up our canvas and draw each side. Additionally, set our gradient fill.


    function drawFace(ctx, coord, popColor, gs, x1, x2, side) {
      // Gradients in our case run either up/down or left right.
      // We have two algorithms depending on whether or not it's a sideways facing piece.
      // Rather than parse the "rgb(r,g,b)" string(popColor) retrieved from elsewhere, it is simply
      // offset with the config.gradientStop variable to give the illusion that it starts at a darker color.
      var lineargradient;

      if (side) {
        lineargradient = ctx.createLinearGradient(coord[x1][X] + gs, config.vanishingPoint.y, config.vanishingPoint.x, config.vanishingPoint.y);
      } else {
        lineargradient = ctx.createLinearGradient(coord[UPPER_LEFT][X], coord[LOWER_RIGHT][Y] + gs, coord[UPPER_LEFT][X], config.vanishingPoint.y);
      }

      lineargradient.addColorStop(0, popColor);
      lineargradient.addColorStop(1, 'black');
      ctx.fillStyle = lineargradient;
      ctx.beginPath(); // Draw from one corner to the vanishingPoint, then to the other corner, and apply a stroke and a fill.

      ctx.moveTo(coord[x1][X], coord[x1][Y]);
      ctx.lineTo(config.vanishingPoint.x, config.vanishingPoint.y);
      ctx.lineTo(coord[x2][X], coord[x2][Y]);
      if (config.stroke) ctx.stroke();
      ctx.fill();
    } // Pretty self-explanatory.


    const clearCanvas = canvas => {
      if (canvas.getContext) {
        let ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);
      }
    }; // Sort our array of elements to be drawn using the Painter's algorithm.
    // Draws objects from furthest out to closest in.


    const sortByDistance = (a, b) => a.distance < b.distance ? 1 : a.distance > b.distance ? -1 : 0; // Our main function. Loops through each element given the "pop" class, and gets
    // necessary information: offset, dimensions, color, and the vanishingPoint of the document.


    function draw(canvas, ctx, elProps) {
      let elements = [];
      let isSide = true;

      for (let a = 0; a < elProps.length; a++) {
        elements[elements.length] = new GetElemProperties(elProps[a]);
      }

      elements.sort(sortByDistance);
      clearCanvas(canvas);

      for (let a = 0; a < elements.length; a++) {
        // In the following conditional statements, we're testing to see which direction faces should be drawn,
        // based on a 1-point perspective drawn from the vanishingPoint. In the first statement, we're testing to see
        // if the lower-left hand corner coord[3] is higher on the screen than the vanishingPoint. If so, we set it's gradient
        // starting position to start at a point in space 60pixels higher(-60) than the actual side, and we also
        // declare which corners make up our face, in this case the lower two corners, coord[3], and coord[2].
        if (elements[a].bottomFace) {
          drawFace(ctx, elements[a].coord, elements[a].popColor, config.gradientStop * -1, LOWER_LEFT, LOWER_RIGHT);
        }

        if (elements[a].topFace) {
          drawFace(ctx, elements[a].coord, elements[a].popColor, config.gradientStop, UPPER_LEFT, UPPER_RIGHT);
        }

        if (!config.noSides) {
          if (elements[a].leftFace) {
            drawFace(ctx, elements[a].coord, elements[a].popColor, config.gradientStop, UPPER_LEFT, LOWER_LEFT, isSide);
          }

          if (elements[a].rightFace) {
            drawFace(ctx, elements[a].coord, elements[a].popColor, config.gradientStop * -1, UPPER_RIGHT, LOWER_RIGHT, isSide);
          }
        }
      }

      window.requestAnimationFrame(draw);
    }

    function canvasManager() {
      // Inject our canvas into the "background".
      var doc = document;
      var wrapper = doc.getElementById('wrapper');
      var background = doc.createElement('div');
      var newCanvas = doc.createElement('canvas');
      newCanvas.setAttribute('width', config.width || d.clientWidth);
      newCanvas.setAttribute('height', config.height || getDocHeight());
      newCanvas.setAttribute('id', config.canvasID);
      background.setAttribute('id', 'background');
      background.appendChild(newCanvas);
      wrapper.parentNode.insertBefore(background, wrapper);
      let elProps = document.querySelectorAll(config.popoutSelector);
      let canvas = document.getElementById(config.canvasID);
      let ctx;
      if (canvas.getContext) ctx = canvas.getContext('2d');

      const evDraw = () => draw(canvas, ctx, elProps); // we need to refresh the canvas if colors change, such as with a hover event.


      doc.addEventListener('DOMContentLoaded', () => {
        doc.querySelectorAll('a.pop').forEach(el => {
          ['click', 'focusin', 'focusout', 'mouseenter', 'mouseleave'].forEach(ev => el.addEventListener(ev, evDraw));
        });
      }); // Resize the canvas when the window size changes

      window.addEventListener('resize', () => {
        if (config.vanishingPoint.recalc) setMidPoint();
        canvas.setAttribute('width', d.clientWidth);
        canvas.setAttribute('height', config.height || getDocHeight());
        evDraw();
      });
      window.addEventListener('scroll', () => {
        if (config.vanishingPoint.recalc) setMidPoint();
        evDraw();
      });
      evDraw();
    }

    canvasManager();
  };

  var __constructor = function () {};

  _$0.popout = _4;
  var _5 = _$2;

  var _1 = (__constructor.prototype = _5, new __constructor());

  $$0.value = "POPOUT_cfg is not defined", _$4(_1, "message", $$0);
  $$0.value = "ReferenceError: POPOUT_cfg is not defined\n    at js\\popout.js:249:8", _$4(_1, "stack", $$0);
  throw _1;
}).call(this);
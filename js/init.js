// Configure Popout by defining these properties. Default values will be used where none are defined.
// Note: If defining midPoint, BOTH 'x' and 'y' MUST BE DEFINED.

window.POPOUT_cfg = {
  hooks: {
    canvas: 'depth',
    popout: '.pop'
  },
  gradient: {
    stop: 160
  },
  stroke: false
}

self.addEventListener('install', function (e) {
  e.waitUntil(
    caches.open('popout').then(function (cache) {
      return cache.addAll([
        '/',
        '/index.html',
        '/css/style.css',
        '/css/color.css',
        'css/typo.css',
        '/js/init.js',
        '/js/popout.js'
      ])
    })
  )
})

self.addEventListener('fetch', function (event) {
  console.log(event.request.url)
  event.respondWith(
    caches.match(event.request).then(function (response) {
      return response || fetch(event.request)
    })
  )
})
